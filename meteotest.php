<?php
$apiKey = "18e81bcbd64836fdceda9ce1e8bb381e";
$openWeatherMapUrl = "http://api.openweathermap.org/data/2.5/forecast?q=Somalia&appid={$apiKey}&units=metric&lang=it";

$ch = curl_init();

curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $openWeatherMapUrl);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$response = curl_exec($ch);

curl_close($ch);
$data = json_decode($response);
$giorno1 = $data->list[0];
$giornoprimo_successivo = $data->list[13];
$giornosecondo_successivo = $data->list[21];
$giornoterzo_successivo = $data->list[29];
$currentTime = time();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

<link href="https://fonts.googleapis.com/css2?family=Material+Icons"
rel="stylesheet">
    <link rel="stylesheet" href="./style.css">

</head>
<body>
    <div  class="weather-card">
        <div>
            <img src="<?php echo "http://openweathermap.org/img/wn/{$giorno1->weather[0]->icon}@2x.png" ?>" alt="<?php echo $data->weather[0]->description; ?>" title="<?php echo $data->weather[0]->description; ?>" />
            <div class="main-weather">
                <h1>Somalia</h1>
                <p><?php echo $giorno1->weather[0]->main; ?></p>
                <p><?php echo date("l g:i a", $giorno1->dt); ?> - <?php echo date("jS F, Y", $giorno1->dt); ?></p>
            </div>
        </div>
        <div>
            <div class="temperature">
                <span><span class="material-icons temp max">thermostat</span>Temp. Max: <?php echo $giorno1->main->temp_max; ?>°F</span>
                <span><span class="material-icons temp min">thermostat</span>Temp. Min: <?php echo $giorno1->main->temp_min; ?>°F</span>
            </div>
            <div class="other-data">
                <span><span class="material-icons">water_drop</span>Umidità: <?php echo $giorno1->main->humidity; ?> %</span>
                <span><span class="material-icons">air</span>Vento: <?php echo $giorno1->wind->speed; ?> mph</span>
            </div>
        </div>
    </div>
</body>
</html>


<body>
    <div  class="weather-card">
        <div>
            <img src="<?php echo "http://openweathermap.org/img/wn/{$giornoprimo_successivo->weather[13]->icon}@2x.png" ?>" alt="<?php echo $data->weather[13]->description; ?>" title="<?php echo $data->weather[13]->description; ?>" />
            <div class="main-weather">
                <h1>Somalia</h1>
                <p><?php echo $giornoprimo_successivo->weather[13]->main; ?></p>
                <p><?php echo date("l g:i a", $giornoprimo_successivo->dt); ?> - <?php echo date("jS F, Y", $giornoprimo_successivo->dt); ?></p>
            </div>
        </div>
        <div>
            <div class="temperature">
                <span><span class="material-icons temp max">thermostat</span>Temp. Max: <?php echo $giornoprimo_successivo->main->temp_max; ?>°F</span>
                <span><span class="material-icons temp min">thermostat</span>Temp. Min: <?php echo $giornoprimo_successivo->main->temp_min; ?>°F</span>
            </div>
            <div class="other-data">
                <span><span class="material-icons">water_drop</span>Umidità: <?php echo $giornoprimo_successivo->main->humidity; ?> %</span>
                <span><span class="material-icons">air</span>Vento: <?php echo $giornoprimo_successivo->wind->speed; ?> mph</span>
            </div>
        </div>
    </div>
</body>
</html>


<body>
    <div  class="weather-card">
        <div>
            <img src="<?php echo "http://openweathermap.org/img/wn/{$giornosecondo_successivo->weather[21]->icon}@2x.png" ?>" alt="<?php echo $data->weather[21]->description; ?>" title="<?php echo $data->weather[21]->description; ?>" />
            <div class="main-weather">
                <h1>Somalia</h1>
                <p><?php echo $giornosecondo_successivo->weather[21]->main; ?></p>
                <p><?php echo date("l g:i a", $giornosecondo_successivo->dt); ?> - <?php echo date("jS F, Y", $giornosecondo_successivo->dt); ?></p>
            </div>
        </div>
        <div>
            <div class="temperature">
                <span><span class="material-icons temp max">thermostat</span>Temp. Max: <?php echo $giornosecondo_successivo->main->temp_max; ?>°F</span>
                <span><span class="material-icons temp min">thermostat</span>Temp. Min: <?php echo $giornosecondo_successivo->main->temp_min; ?>°F</span>
            </div>
            <div class="other-data">
                <span><span class="material-icons">water_drop</span>Umidità: <?php echo $giornosecondo_successivo->main->humidity; ?> %</span>
                <span><span class="material-icons">air</span>Vento: <?php echo $giornosecondo_successivo->wind->speed; ?> mph</span>
            </div>
        </div>
    </div>
</body>
</html>



<body>
    <div  class="weather-card">
        <div>
            <img src="<?php echo "http://openweathermap.org/img/wn/{$giornoterzo_successivo->weather[29]->icon}@2x.png" ?>" alt="<?php echo $data->weather[29]->description; ?>" title="<?php echo $data->weather[29]->description; ?>" />
            <div class="main-weather">
                <h1>Somalia</h1>
                <p><?php echo $giornoterzo_successivo->weather[29]->main; ?></p>
                <p><?php echo date("l g:i a", $giornoterzo_successivo->dt); ?> - <?php echo date("jS F, Y", $giornoterzo_successivo->dt); ?></p>
            </div>
        </div>
        <div>
            <div class="temperature">
                <span><span class="material-icons temp max">thermostat</span>Temp. Max: <?php echo $giornoterzo_successivo->main->temp_max; ?>°F</span>
                <span><span class="material-icons temp min">thermostat</span>Temp. Min: <?php echo $giornoterzo_successivo->main->temp_min; ?>°F</span>
            </div>
            <div class="other-data">
                <span><span class="material-icons">water_drop</span>Umidità: <?php echo $giornoterzo_successivo->main->humidity; ?> %</span>
                <span><span class="material-icons">air</span>Vento: <?php echo $giornoterzo_successivo->wind->speed; ?> mph</span>
            </div>
        </div>
    </div>
</body>
</html>
